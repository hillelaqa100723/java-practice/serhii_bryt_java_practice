package org.bryt.lection9;

public class MyArrays {
    public static void main(String[] args) {
        int[][] array =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };

        System.out.print("Головна діагональ: ");
        printArray(mainDiagonal(array));
        System.out.println();

        System.out.print("Побічна діагональ: ");
        printArray(subDiagonal(array));
        System.out.println();

        System.out.print("Сума по рядках:  " );
        printArray(sumsInRows(array));

    }

    public static int[] mainDiagonal(int[][] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i][i];
        }
        return result;
    }

    public static int[] subDiagonal(int[][] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[array.length - 1 - i][i];
        }
        return result;
    }

    public static int[] sumsInRows(int[][] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                result[i] += array[i][i];
            }
        }
        return result;
    }

    public static void printArray(int[] array) {
        for (int elem : array)
            System.out.print(elem + " ");
    }


}
