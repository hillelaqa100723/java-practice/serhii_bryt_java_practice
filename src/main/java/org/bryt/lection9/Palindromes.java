package org.bryt.lection9;

public class Palindromes {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 3, 2};
        System.out.printf("This array is %s palindrome",
                isPalindrome(array) ? "" : "not");


    }

    public static boolean isPalindrome(String str) {
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindrome(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] != array[array.length - 1 - i]) {
                return false;
            }
        }
        return true;
    }
}
