package org.bryt.lection7;

import java.util.HashMap;
import java.util.Map;

public class MyMaps {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Bryt Serhii");
        map.put(2, "Bryt Mark");
        map.put(3, "Voronko Svitlana");
        map.put(4, "Bryt Artem");
        map.put(5, "Bryt Mariia");
        map.put(6, "Bryt Serhii");

        System.out.println(map);

        map.put(2, "Bryt Svitlana");

        System.out.println(map);

        System.out.println(map.get(3));

        System.out.println(map.get(10));

        for (Map.Entry<Integer, String> entry : map.entrySet()){
            System.out.println("Index: " + entry.getKey() + " ==> " + entry.getValue());
        }

        for (Integer key: map.keySet()){
            System.out.print(key + " ");
        }

        System.out.println();
        for (String str: map.values()){
            System.out.print(str + ", ");
        }

        System.out.println();

        if (map.containsKey(10)){
            System.out.println("Yes!!");
        } else {
            System.out.println("No!!!");
        }

        if (map.get(10) != null){
            System.out.println("Yes!!");
        } else {
            System.out.println("No!!!");
        }




    }
}
