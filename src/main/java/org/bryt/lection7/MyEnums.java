package org.bryt.lection7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyEnums {

    public static void main(String[] args) {
        DayOfWeek dayOfWeek = DayOfWeek.SATURDAY;

        System.out.println(dayOfWeek.myOrdinal());

        System.out.println(dayOfWeek.getValue());

        String dayOfWeekString = "MonDay";

        DayOfWeek dayOfWeek1 = DayOfWeek.valueOf(dayOfWeekString.toUpperCase());
        System.out.println(dayOfWeek1);

        switch (dayOfWeek) {
            case MONDAY:
            case TUESDAY:
            case THURSDAY:
            case WEDNESDAY:
            case FRIDAY:
                System.out.println("This is work day!");
                break;
            case SUNDAY:
            case SATURDAY:
                System.out.println("This is weekend!!!");
        }

        System.out.println(Arrays.asList(DayOfWeek.values()));
    }

    enum DayOfWeek {
        MONDAY("Понеділок", 1),
        TUESDAY("Вівторок", 2),
        WEDNESDAY("Середа", 3),
        THURSDAY("Четвер", 4),
        FRIDAY("Пятниця", 5),
        SATURDAY("Субота", 6),
        SUNDAY("Неділя", 7);

        private String value;
        private int myOrdinal;

        DayOfWeek(String value, int myOrdinal) {
            this.value = value;
            this.myOrdinal = myOrdinal;
        }

        public String getValue() {
            return value;
        }

        public int myOrdinal() {
            return myOrdinal;
        }

//        @Override
//        public String toString() {
//            return value;
//        }
    }
}
