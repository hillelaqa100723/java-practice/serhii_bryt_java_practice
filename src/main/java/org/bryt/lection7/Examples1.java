package org.bryt.lection7;

import java.util.*;

public class Examples1 {
    public static void main(String[] args) {
        List<Map<String, Object>> userList = new ArrayList<>();

        userList.add(createUser("Serhii", "Bryt", 42));
        userList.add(createUser("Svitlana", "Voronko", 35));
        userList.add(createUser("Artem", "Bryt", 11));

        System.out.println(userList);

        for (Map<String, Object> user : userList) {
            printUser(user);
        }

        for (Map<String, Object> user : userList) {
            if ((Integer)user.get("age") <= 40) {
                printUser(user);
            }
        }
    }

    public static Map<String, Object> createUser(String name, String lastName, Integer age) {
        Map<String, Object> user = new HashMap<>();
        user.put("name", name);
        user.put("lastName", lastName);
        user.put("age", age);
        return user;
    }

    private static void printUser(Map<String, Object> user) {
        System.out.println("Name: " + user.get("name"));
        System.out.println("Last name: " + user.get("lastName"));
        System.out.println("Age: " + user.get("age"));
        System.out.println();
    }
}
