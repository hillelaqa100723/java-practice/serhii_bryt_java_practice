package org.bryt.lection7;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MySet {

    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();

        set.add(1);
        set.add(2);
        set.add(3);
        set.add(1);

        System.out.println(set.size());
        System.out.println(set);

        for (Integer element: set){
            System.out.print(element + " ");
        }

        List<Integer> list = List.of(1,5,2,6,1,5,6,7,8,9,9);
        set.clear();
        set.addAll(list);
        System.out.println();
        System.out.println(set);

    }
}
