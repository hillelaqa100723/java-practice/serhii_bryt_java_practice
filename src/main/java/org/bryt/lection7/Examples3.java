package org.bryt.lection7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Examples3 {
    public static void main(String[] args) {
        Map<Integer, Map<String, Object>> userList = new HashMap<>();

        userList.put(1, createUser("Serhii", "Bryt", 42));
        userList.put(2, createUser("Svitlana", "Voronko", 35));
        userList.put(3, createUser("Artem", "Bryt", 11));

        System.out.println(userList);

        for (Integer key: userList.keySet()){
            printUser(userList.get(key));
        }

        for (Map.Entry<Integer, Map<String, Object>> entry: userList.entrySet()){
            printUser(entry.getValue());
        }



        for (Map.Entry<Integer, Map<String, Object>> entry: userList.entrySet()) {
            if ((Integer)entry.getValue().get("age") <= 40) {
                printUser(entry.getValue());
            }
        }
    }

    public static Map<String, Object> createUser(String name, String lastName, Integer age) {
        Map<String, Object> user = new HashMap<>();
        user.put("name", name);
        user.put("lastName", lastName);
        user.put("age", age);
        return user;
    }

    private static void printUser(Map<String, Object> user) {
        System.out.println("Name: " + user.get("name"));
        System.out.println("Last name: " + user.get("lastName"));
        System.out.println("Age: " + user.get("age"));
        System.out.println();
    }
}
