package org.bryt.lection7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Examples {
    /*
    name, lastName, age
     */

    public static void main(String[] args) {
        List<Map<String, String>> userList = new ArrayList<>();

        userList.add(createUser("Serhii", "Bryt", "42"));
        userList.add(createUser("Svitlana", "Voronko", "35"));
        userList.add(createUser("Artem", "Bryt", "11"));

        System.out.println(userList);

        for (Map<String, String> user : userList) {
            printUser(user);
        }

        for (Map<String, String> user : userList) {
            if (Integer.parseInt(user.get("age")) <= 40) {
                printUser(user);
            }
        }
    }

    public static Map<String, String> createUser(String name, String lastName, String age) {
        Map<String, String> user = new HashMap<>();
        user.put("name", name);
        user.put("lastName", lastName);
        user.put("age", age);
        return user;
    }

    private static void printUser(Map<String, String> user) {
        System.out.println("Name: " + user.get("name"));
        System.out.println("Last name: " + user.get("lastName"));
        System.out.println("Age: " + user.get("age"));
        System.out.println();
    }
}
