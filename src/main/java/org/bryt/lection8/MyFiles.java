package org.bryt.lection8;

import java.io.File;
import java.io.IOException;

public class MyFiles {
    public static void main(String[] args) throws IOException {
        File file = new File("files/file.txt");
        File file1 = new File("files", "file1.txt");
        file1.deleteOnExit();

        System.out.println(file.length());

        System.out.println(file.exists());
        System.out.println(file.isFile());
        System.out.println(new File(file.getParent()).isDirectory());

        System.out.println(file.getAbsolutePath());

        System.out.println(file.getName());

        System.out.println(file1.createNewFile());
    }
}
