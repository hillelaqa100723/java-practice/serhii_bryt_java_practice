package org.bryt.lection8;

import com.github.javafaker.Faker;

import java.util.Date;
import java.util.List;

public class MyFaker {
    public static void main(String[] args) {
        Faker faker = new Faker();

        for (int i = 0; i < 20; i++) {
            String firstName = faker.address().firstName();
            String lastName = faker.address().lastName();
            Date birthday = faker.date().birthday();

            System.out.println(firstName + " " + lastName + " " + birthday.toString());
        }

        System.out.println();
        System.out.println();
        List<String> paragraphs = faker.lorem().paragraphs(10);
        for (String par: paragraphs){
            System.out.println(par + '\n');
        }


    }
}
