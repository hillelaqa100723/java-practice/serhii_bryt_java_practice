package org.bryt.lection8;

import com.github.javafaker.Faker;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;

public class WriteFile {
    //BufferedWriter
    //FileUtils
    //Files
    public static void main(String[] args) throws IOException {
        Faker faker = new Faker();
        String paragraph = faker.lorem().paragraph(10);
        File fileToWrite = new File("files/file22.txt");
        fileToWrite.createNewFile();
        //writeWithBufferWriter(fileToWrite, paragraph, false);
        //writeWithFileUtils(fileToWrite, paragraph, false);
        writeWithFiles(fileToWrite, paragraph, false);
    }

    public static void writeWithBufferWriter(File file, String str, boolean append) throws IOException {
        FileWriter fileWriter = new FileWriter(file, append);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        for (Character ch : str.toCharArray()) {
            bufferedWriter.write(ch);
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    public static void writeWithFileUtils(File file, String str, boolean append) throws IOException {
        FileUtils.writeStringToFile(file, str, Charset.defaultCharset(), append);
    }

    public static void writeWithFiles(File file, String str, boolean append) throws IOException {
        StandardOpenOption standardOpenOption = append? StandardOpenOption.APPEND: StandardOpenOption.WRITE;
        Files.writeString(file.toPath(), str, standardOpenOption);
    }
}
