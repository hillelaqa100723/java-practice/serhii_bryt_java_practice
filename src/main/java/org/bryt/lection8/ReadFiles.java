package org.bryt.lection8;


import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Scanner;

public class ReadFiles {
    //BufferedReader
    //Scanner
    //FileUtils
    //Files

    public static void main(String[] args) throws IOException {
        File file = new File("files/file.txt");
        System.out.println(readFileWithBufferReader(file));

        System.out.println();
        System.out.println();

        System.out.println(readWithScanner(file));

        System.out.println();
        System.out.println();

        System.out.println(readFileWithFileUtils(file));

        System.out.println();
        System.out.println();

        System.out.println(readWithFiles(file));



    }

    public static String readFileWithBufferReader(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        StringBuffer stringBuffer = new StringBuffer();
        int ch;
        while ((ch = bufferedReader.read()) != -1) {
            stringBuffer.append((char) ch);
        }
        return stringBuffer.toString();
    }

    public static String readWithScanner(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        scanner.useDelimiter(" ");
        StringBuffer stringBuffer = new StringBuffer();
        while (scanner.hasNext()) {
            stringBuffer.append(scanner.next()).append(" ");
        }
        return stringBuffer.toString();
    }

    public static String readFileWithFileUtils(File file) throws IOException {
        return FileUtils.readFileToString(file, Charset.defaultCharset());
    }

    public static String readWithFiles(File file) throws IOException {
       return Files.readString(file.toPath());
    }
}
