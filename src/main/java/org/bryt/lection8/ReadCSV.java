package org.bryt.lection8;

import org.apache.commons.io.FileUtils;
import org.bryt.lection4.ForOperator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class ReadCSV {
    public static void main(String[] args) throws IOException {
        File fileCsv = new File("files/users.csv");
        List<String> strings = FileUtils.readLines(fileCsv, Charset.defaultCharset());
        List<Map<String, Object>> users = new ArrayList<>();

        for (String str : strings.subList(1, strings.size())) {
            String[] split = str.split(",");
            Map<String, Object> user = new HashMap<>();
            user.put("firstName", split[0].trim());
            user.put("lastName", split[1].trim());
            user.put("gender", split[3].trim());
            user.put("age", Integer.parseInt(split[2].trim()));
            users.add(user);
        }

        for (Map<String, Object> user: users){
            System.out.println("First name: " + user.get("firstName"));
            System.out.println("Last name: " + user.get("lastName"));
            System.out.println("Age: " + user.get("age"));
            System.out.println("Gender: " + user.get("gender"));
            System.out.println();
        }

    }
}
