package org.bryt.lection10.items;

import org.apache.commons.lang3.RandomStringUtils;
import org.bryt.lection10.items.Item;
import org.bryt.lection10.items.Item2;

public class Bread extends Item {

    private String name;

    public Bread() {
        super();
        this.name = RandomStringUtils.randomAlphabetic(10);
    }

    public Bread(Double price, Double weight, Long id, String name) {
        super(price, weight, id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
