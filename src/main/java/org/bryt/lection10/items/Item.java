package org.bryt.lection10.items;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class Item {
    private Double price;
    private Double weight;
    private Long id;

    public Item() {
        this(10.0, 10.0, RandomUtils.nextLong());
    }

    public Item(Double price, Double weight, Long id) {
        this.price = price;
        this.weight = weight;
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
