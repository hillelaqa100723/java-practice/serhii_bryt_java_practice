package org.bryt.lection10.animals;

public class Tiger extends Cat{
    public Tiger(Double weight, Integer legs, Integer eyes) {
        super(weight, legs, eyes);
    }

    public Tiger(Double weight, Integer legs, Integer eyes, String name) {
        super(weight, legs, eyes, name);
    }

    @Override
    public void say() {
        System.out.println("GRRRR!!!");
    }
}
