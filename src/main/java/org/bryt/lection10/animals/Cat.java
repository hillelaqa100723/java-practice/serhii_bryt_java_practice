package org.bryt.lection10.animals;

import org.apache.commons.lang3.RandomStringUtils;

public class Cat extends Animal{

    private String name = "Cat" + RandomStringUtils.randomNumeric(3);

    public Cat(Double weight, Integer legs, Integer eyes) {
        super(weight, legs, eyes);
    }

    public Cat(Double weight, Integer legs, Integer eyes, String name) {
        super(weight, legs, eyes);
        this.name = name;
    }

    @Override
    public void say() {
        System.out.println("Meowww!!!!");
    }

    @Override
    public String toString() {
        return "Cat{" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
