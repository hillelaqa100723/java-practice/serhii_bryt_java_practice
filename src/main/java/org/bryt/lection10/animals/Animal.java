package org.bryt.lection10.animals;

public abstract class Animal {
    private Double weight;
    private Integer legs;
    private Integer eyes;

    private boolean canRun;

    public abstract void say();

    public Animal(Double weight, Integer legs, Integer eyes) {
        this.weight = weight;
        this.legs = legs;
        this.eyes = eyes;
        this.canRun = legs >= 2;
    }

    public boolean canRun(){
        return canRun;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getLegs() {
        return legs;
    }

    public void setLegs(Integer legs) {
        this.legs = legs;
    }

    public Integer getEyes() {
        return eyes;
    }

    public void setEyes(Integer eyes) {
        this.eyes = eyes;
    }

    @Override
    public String toString() {
        return "Animal{" +
               "weight=" + weight +
               ", legs=" + legs +
               ", eyes=" + eyes +
               ", canRun=" + canRun +
               '}';
    }
}
