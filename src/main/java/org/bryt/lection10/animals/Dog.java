package org.bryt.lection10.animals;

public class Dog extends Animal{
    public Dog(Double weight, Integer legs, Integer eyes) {
        super(weight, legs, eyes);
    }

    @Override
    public void say() {
        System.out.println("WoooFFF!!!");
    }
}
