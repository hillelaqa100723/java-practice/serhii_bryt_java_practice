package org.bryt.lection10;

import org.bryt.lection10.animals.Animal;
import org.bryt.lection10.animals.Cat;
import org.bryt.lection10.animals.Tiger;
import org.bryt.lection10.items.Bread;

public class Item3 {
    private Double price;
    public Double weight;
    protected Long id;

    public void method1() {
        Bread bread = new Bread();
    }

    public static void main(String[] args) {
        Animal cat = new Cat(10.0, 4, 2, "Кицюня");
        cat.say();
        System.out.println(cat);

        Animal tiger = new Tiger(10.0, 4, 2, "Кицюня");

        tiger.say();

    }
}
