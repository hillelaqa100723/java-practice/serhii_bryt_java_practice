package org.bryt.lection3;

import javax.swing.*;

public class Calculator {
    public static void main(String[] args) {
        int a = 15;
        int b = 0;

        int rez = plus(a, b);

        prinResult("Plus: ", plus(a, b));
        prinResult("Minus: ", minus(a, b));
        prinResult("Mult: ", mult(a, b));
        prinResult("Div: ", div(a, b));
        prinResult("Rem: ", rem(a, b));
    }

    public static void prinResult(String comment, int value) {
        System.out.println(comment + value);
    }

    /**
     * Adding one inter to another
     * @param p first integer
     * @param r second integer
     * @return result as integer
     */
    public static int plus(int p, int r) {
        return p + r;
    }

    public static int plus(double p, int r) {
        return (int) p + r;
    }

    public static int minus(int a, int b) {
        return a - b;
    }

    public static int mult(int a, int b) {
        return a * b;
    }

    public static int div(int a, int b) {
        if (b != 0) {
            return a / b;
        } else {
            System.out.println("You cannot divide on 0!");
            return -10000;
        }
    }

    public static int rem(int a, int b) {
        if (b != 0) {
            return a % b;
        } else {
            System.out.println("You cannot divide on 0!");
            return -10000;
        }
    }


}





