package org.bryt.lection3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringProgram {

    public static void main(String[] args) {
        String str = "Hello world!!!";
        System.out.println(str);

        String str1 = "    " + str + " New year!!  !     ";
        System.out.println(str1);

        System.out.println(str1.length());

        System.out.println(str1.toLowerCase());
        System.out.println(str1.toUpperCase());
        System.out.println(str1);
        System.out.println(str1.trim());
        System.out.println(str1.trim().charAt(0));
        System.out.println(str1.trim().substring(6));
        System.out.println(str1.trim().substring(6, 11));



        String str2 = "hello world!!";
        str2 = str2 + "!";
        System.out.println(str1.trim().replace(" ", ""));
        System.out.println(str1.trim().startsWith("Hell"));
        System.out.println(str.equals(str2));

        System.out.println(str.equalsIgnoreCase(str2));

        System.out.println(str.compareTo(str2));

        char ch = 'r';
        System.out.println((int) ch);
        System.out.println((int) 'R');

        List<String> list = new ArrayList<>();
        list.addAll(List.of("Hello", "HeLLo", "hEllo", "Hello"));

        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);



    }
}
