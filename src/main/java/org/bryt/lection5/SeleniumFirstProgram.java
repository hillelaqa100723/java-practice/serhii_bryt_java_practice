package org.bryt.lection5;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class SeleniumFirstProgram {
    public static void main(String[] args) throws InterruptedException {
        String value = "Hillel";
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.google.com/");
        driver.findElement(By.name("q")).sendKeys("Hillel It School", Keys.ENTER);
        Thread.sleep(5000);
        List<WebElement> elements = driver.findElements(By.xpath("//*[@id='res']//a/h3"));
        List<String> list = new ArrayList<>();
        for(WebElement element:elements){
            list.add(element.getText());
        }

        for (String val: list){
            Assert.assertTrue(val.contains(value));
        }

        driver.quit();

    }
}
