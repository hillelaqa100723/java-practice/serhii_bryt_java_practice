package org.bryt.lection5;

public class IterateArray {
    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println();

        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }

        System.out.println();
//                 i        10 - l
//        1 10     0   9    l - i - 1
//        2 9      1   8    l - i - 1
//        3 8      2   7    l - i - 1
//        4 7
//        5 6

        System.out.println();
        for (int i = 0; i < array.length / 2; i++) {
            System.out.println(array[i] + " " + array[array.length - i - 1]);
        }

        System.out.println();

        for (int i = 0, j = array.length - 1; i < array.length && j > i; i++, j--) {
            System.out.println(array[i] + " " + array[j]);
        }

        System.out.println();

        for (int element: array){
            if (element % 3 == 0){
                System.out.print(element + " ");
            }
        }
    }
}
