package org.bryt.lection5;

public class StringAsArray {
    public static void main(String[] args) {
        String str = "Hello world!!!";

        char[] charArray = str.toCharArray();

        for (int i = 0; i < charArray.length / 2; i++) {
            System.out.println(charArray[i] + " " + charArray[charArray.length - i - 1]);
        }

        System.out.println();

        for (int i = 0; i < str.length() / 2; i++) {
            System.out.println(str.charAt(i) + " " + str.charAt(str.length() - i - 1));
        }

        boolean yes;
        int a = 1;

        if (a > 8) {
            yes = true;
        } else {
            yes = false;
        }

        yes = a > 8 ? true : false;
    }
}
