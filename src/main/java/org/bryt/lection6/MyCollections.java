package org.bryt.lection6;

import java.util.*;

public class MyCollections {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(5);
        System.out.println(integerList);
        integerList.add(1);
        integerList.add(15);

        System.out.println(integerList);

        integerList.add(2, 1);
        System.out.println(integerList);
        System.out.println(integerList.get(0));
        System.out.println(integerList.size());

        System.out.println(integerList);

        List<Integer> integerList1 = List.of(1, 2, 3);
        List<Integer> integerList2 = List.of(3, 2, 1);
        System.out.println(integerList1.equals(integerList2));
        System.out.println(integerList1.hashCode());
        System.out.println(integerList2.hashCode());

        System.out.println(integerList.indexOf(15));
        Collections.sort(integerList);

        System.out.println(integerList);

        Collections.sort(integerList, Collections.reverseOrder());

        System.out.println(integerList);

        integerList.sort(Comparator.naturalOrder());
        System.out.println(integerList);

    }
}
