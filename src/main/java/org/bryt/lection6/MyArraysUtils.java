package org.bryt.lection6;

import java.util.Arrays;

public class MyArraysUtils {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4};
        double[] arrayDouble = {1.0, 2.0, 3.0, 4.0};

        // add at the beginning
        // add at the end
        // insert

        MyArraysUtils.printArray(arrayDouble);

        double[] result = MyArraysUtils.addToTheBeginning(arrayDouble, 99);
        System.out.println();
        MyArraysUtils.printArray(result);
        System.out.println();
        result = MyArraysUtils.addToTheEnd(result, 99);
        MyArraysUtils.printArray(result);
        System.out.println();
        result = MyArraysUtils.insertToArray(result, 3, 11);
        printArray(result);
        System.out.println();
        result = MyArraysUtils.insertToArray(result, 0, 999);
        MyArraysUtils.printArray(result);
        System.out.println();
        result = MyArraysUtils.insertToArray(result, result.length, 999);
        MyArraysUtils.printArray(result);
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.print(array[array.length - 1]);
    }

    public static void printArray(double[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.print(array[array.length - 1]);
    }

    public static int[] addToTheBeginning(int[] sourceArray, int value) {
        return insertToArray(sourceArray, 0, value);
    }

    public static int[] addToTheEnd(int[] sourceArray, int value) {
        return insertToArray(sourceArray, sourceArray.length, value);
    }

    public static double[] addToTheBeginning(double[] sourceArray, double value) {
        return insertToArray(sourceArray, 0, value);
    }

    public static double[] addToTheEnd(double[] sourceArray, double value) {
        return insertToArray(sourceArray, sourceArray.length, value);
    }


    public static int[] insertToArray(int[] sourceArray, int index, int value) {
        if (index > sourceArray.length || index < 0) {
            System.out.println("NO!!!");
            return sourceArray;
        }

        int[] targetArray = new int[sourceArray.length + 1];
        int i = 0;
        for (; i < index; i++) {
            targetArray[i] = sourceArray[i];
        }
        targetArray[i] = value;
        for (; i < targetArray.length; i++) {
            targetArray[i + 1] = sourceArray[i];
        }
        return targetArray;
    }

    public static double[] insertToArray(double[] sourceArray, int index, double value) {
        if (index > sourceArray.length || index < 0) {
            System.out.println("NO!!!");
            return sourceArray;
        }

        double[] targetArray = new double[sourceArray.length + 1];
        int i = 0;
        for (; i < index; i++) {
            targetArray[i] = sourceArray[i];
        }
        targetArray[i] = value;
        for (; i < targetArray.length; i++) {
            targetArray[i + 1] = sourceArray[i];
        }
        return targetArray;
    }


}
