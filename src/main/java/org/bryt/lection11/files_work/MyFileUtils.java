package org.bryt.lection11.files_work;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MyFileUtils {
    public static List<Student> getDataFromFileAsList(File csv) throws IOException {
        List<Student> result = new LinkedList<>();

        List<String> strings = FileUtils.readLines(csv, Charset.defaultCharset());
        for (String str : strings.subList(1, strings.size())) {
            String[] split = str.split(",");

            Student student = new Student();

            Integer id = Integer.parseInt(split[0].trim());
            String firstName = split[1].trim();
            String lastName = split[2].trim();
            Integer age = Integer.parseInt(split[3].trim());
            String group = split[4].trim();
            Gender gender = Gender.getByValue(split[5].trim());

            student
                    .setId(id)
                    .setFirstName(firstName)
                    .setLastName(lastName)
                    .setAge(age)
                    .setGroup(group)
                    .setGender(gender);

            result.add(student);
        }
        return result;
    }

    public static Map<Integer, StudentForMap> getDataFromFileAsMap(File csv) throws IOException {
        Map<Integer, StudentForMap> result = new HashMap<>();
        List<String> strings = FileUtils.readLines(csv, Charset.defaultCharset());
        for (String str : strings.subList(1, strings.size())) {
            var index = Integer.parseInt(str.split(",")[0]);
            StudentForMap student = new StudentForMap(str);
            result.put(index, student);

        }
        return result;
    }

}
