package org.bryt.lection11.files_work;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Program {
    public static void main(String[] args) throws IOException {
        File file = new File("files/Students.csv");
        List<Student> dataFromFile = MyFileUtils.getDataFromFileAsList(file);

        dataFromFile.forEach(System.out::println);

        Map<Integer, StudentForMap> dataFromFileAsMap = MyFileUtils.getDataFromFileAsMap(file);
        System.out.println(dataFromFileAsMap);

        List<Integer> ids = new ArrayList<>();

        for (Map.Entry<Integer, StudentForMap> entry: dataFromFileAsMap.entrySet()){
            if (entry.getValue().getAge() < 40){
                ids.add(entry.getKey());
            }
        }

        System.out.println(ids);


        List<Student> beans = new CsvToBeanBuilder(new CSVReader(new FileReader("files/Students1.csv")))
                .withSkipLines(1)
                .withType(Student.class).build().parse();

        System.out.println(beans);
    }
}
