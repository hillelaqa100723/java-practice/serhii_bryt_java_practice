package org.bryt.lection11.files_work;

public class Student {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private String group;

    private Gender gender;

    public Student() {
    }

    public Student(Integer id, String firstName, String lastName, Integer age, String group, Gender gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.group = group;
        this.gender = gender;
    }

    public Student(String str){
        String[] split = str.split(",");

        id = Integer.parseInt(split[0].trim());
        firstName = split[1].trim();
        lastName = split[2].trim();
        age = Integer.parseInt(split[3].trim());
        group = split[4].trim();
        gender = Gender.getByValue(split[5].trim());
    }

    public Integer getId() {
        return id;
    }

    public Student setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Student setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Student setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public Student setAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public Student setGroup(String group) {
        this.group = group;
        return this;
    }

    public Gender getGender() {
        return gender;
    }

    public Student setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
               "id=" + id +
               ", firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
               ", age=" + age +
               ", group='" + group + '\'' +
               ", gender=" + gender +
               '}';
    }
}
