package org.bryt.lection11.files_work;

public class StudentForMap {
    private String firstName;
    private String lastName;
    private Integer age;
    private String group;

    private Gender gender;

    public StudentForMap() {
    }

    public StudentForMap(String firstName, String lastName, Integer age, String group, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.group = group;
        this.gender = gender;
    }

    public StudentForMap(String str){
        String[] split = str.split(",");

        firstName = split[1].trim();
        lastName = split[2].trim();
        age = Integer.parseInt(split[3].trim());
        group = split[4].trim();
        gender = Gender.getByValue(split[5].trim());
    }


    public String getFirstName() {
        return firstName;
    }

    public StudentForMap setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public StudentForMap setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public StudentForMap setAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public StudentForMap setGroup(String group) {
        this.group = group;
        return this;
    }

    public Gender getGender() {
        return gender;
    }

    public StudentForMap setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
               "  firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
               ", age=" + age +
               ", group='" + group + '\'' +
               ", gender=" + gender +
               '}';
    }
}
