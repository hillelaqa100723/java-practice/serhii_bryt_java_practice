package org.bryt.lection11.files_work;

public enum Gender {
    NA("N"),
    FEMALE("F"),
    MALE("M");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Gender getByValue(String value){
        for (Gender gender: Gender.values()){
            if (gender.getValue().equals(value)){
                return gender;
            }
        }
        return NA;
    }
}
