package org.bryt.lection11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Lottery {
    public static void main(String[] args) {
        List<Integer> digits = new ArrayList<>();
        for (int i = 1; i < 37; i++) {
            digits.add(i);
        }

        List<Integer> result = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(digits.size());
            result.add(digits.get(index));
            digits.remove(index);
        }

        System.out.println(result);
    }
}
