package org.bryt.lection11;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class CalculatorWithFile {
    public static void main(String[] args) {
        List<String> data = List.of("5.9", "div", "-5.0");
        System.out.println(prepareResultString(data, calculate(data)));
    }

    public static List<String> getDataFromFile(File file){
        try {
            List<String> strings = Files.readAllLines(file.toPath());
            String[] split = strings.get(0).split(",");
            return List.of(split[0].trim(), split[1].toUpperCase().trim(), split[2].trim());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Double calculate(List<String> data){
        Double operand1 = Double.parseDouble(data.get(0));
        Double operand2 = Double.parseDouble(data.get(2));
        Operation operation = Operation.valueOf(data.get(1).toUpperCase());

        switch (operation){
            case PLUS: {
                return operand1 + operand2;
            }
            case MULT:{
                return operand1 * operand2;
            }
            case MINUS:{
                return operand1 - operand2;
            }
            case DIV: {
                if (operand2 == 0){
                    System.out.println("Cannot divide by O!");
                    return null;
                }
                return operand1 / operand2;
            }
        }
        return null;
    }

    public static String prepareResultString(List<String> data, Double result){
        Double operand1 = Double.parseDouble(data.get(0));
        Double operand2 = Double.parseDouble(data.get(2));
        Operation operation = Operation.valueOf(data.get(1).toUpperCase());

        String resultStr = operand1 + " ";
        switch (operation){
            case PLUS:{
                resultStr += "+ ";
                break;
            }
            case MULT:{
                resultStr += "* ";
                break;
            }
            case DIV:{
                resultStr += "/ ";
                break;
            }
            case MINUS:{
                resultStr += "- ";
                break;
            }
        }
        resultStr += operand2 + " = " + result;
        return resultStr;
    }

    public static String prepareResultString1(List<String> data, Double result){
        Double operand1 = Double.parseDouble(data.get(0));
        Double operand2 = Double.parseDouble(data.get(2));
        Operation operation = Operation.valueOf(data.get(1).toUpperCase());

        String operationStr = "";

        switch (operation){
            case PLUS:{
                operationStr = "+";
                break;
            }
            case MULT:{
                operationStr = "*";
                break;
            }
            case DIV:{
                operationStr = "/";
                break;
            }
            case MINUS:{
                operationStr = "-";
                break;
            }
        }

        return operand1 + " " + operationStr + " " + operand2 + " = " + result;

    }

    public static String prepareResultString2(List<String> data, Double result){
        Double operand1 = Double.parseDouble(data.get(0));
        Double operand2 = Double.parseDouble(data.get(2));
        Operation operation = Operation.valueOf(data.get(1).toUpperCase());

        return operand1 + " " + operation.getValue() + " " + operand2 + " = " + result;
    }

    public static String prepareResultString3(List<String> data, Double result){
        return "%s %s %s = %s".formatted(
                Double.parseDouble(data.get(0)),
                Operation.valueOf(data.get(1).toUpperCase()).getValue(),
                Double.parseDouble(data.get(2)),
                result);
    }

    enum Operation{
        PLUS("+"),
        MINUS("-"),
        DIV("/"),
        MULT("*");

        private String value;

        Operation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
