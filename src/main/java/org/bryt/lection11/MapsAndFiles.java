package org.bryt.lection11;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsAndFiles {

    public static void main(String[] args) {
        Map<Integer, String> dataFromFile = getDataFromFile(new File("files/data.txt"));
        System.out.println(getDataById(dataFromFile, 3522));
        System.out.println(getNumberOfOccurrences(dataFromFile, "Petrov"));
    }


    public static Map<Integer, String> getDataFromFile(File dataFile) {
        try {
            List<String> users = Files.readAllLines(dataFile.toPath());
            Map<Integer, String> usersMap = new HashMap<>();

            for (String user : users) {
                String[] split = user.split(",");
                Integer id = Integer.parseInt(split[0].trim());
                String lastName = split[1].trim();

                usersMap.put(id, lastName);
            }
            return usersMap;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getDataById(Map<Integer, String> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName) {
        int counter = 0;

        for (Map.Entry<Integer, String> entry : mapData.entrySet()) {
            String lastNameTemp = entry.getValue().split(" ")[0].trim();
            if (lastNameTemp.equals(lastName)) {
                counter++;
            }
        }
        return counter;
    }
}
