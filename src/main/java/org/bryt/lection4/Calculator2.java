package org.bryt.lection4;

public class Calculator2 {
    public static void main(String[] args) {
        int a = 15;
        int b = 8;
        // + - / * %
        char operation = '+';

        int rez = 0;


        switch (operation) {
            case '+':
                rez = a + b;
                break;
            case '-':
                rez = a - b;
                break;
            case '*':
                rez = a * b;
                break;
            case '/':
                if (b != 0) {
                    rez = a / b;
                } else {
                    rez = -100000;
                    System.out.println("Cannot divide by 0!");
                }
                break;
            case '%':
                if (b != 0) {
                    rez = a % b;
                } else {
                    rez = -100000;
                    System.out.println("Cannot divide by 0!");
                }
                break;
            default:
                System.out.println("Operation is not supported!!!");
        }
        System.out.printf("Result: %s", rez);

    }


}
