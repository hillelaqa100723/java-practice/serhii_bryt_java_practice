package org.bryt.lection4;

public class IfWithRanges {
    public static void main(String[] args) {
        int point = 0;

        if (point <= -10) {
            System.out.println("point <= 10");
        } else if (point > -10 && point < -5) {
            System.out.println("point > -10 && point < -5");
        } else if (point >= -5 && point < 0) {
            System.out.println("point >= -5 && point < 0");
        } else if (point >= 0 && point <= 5) {
            System.out.println("point >= 0 && point <= 5");
        } else if (point > 5 && point <= 10){
            System.out.println("point > 5 && point <= 10");
        } else {
            System.out.println("point > 10");
        }
    }

}
