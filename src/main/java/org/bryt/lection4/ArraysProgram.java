package org.bryt.lection4;

public class ArraysProgram {
    public static void main(String[] args) {
        int[] array = {1, 5, 8, 3};
        System.out.println(array[0]);

        int[] array1 = new int[4];
        System.out.println(array1[0]);
        array1[0] = 5;
        System.out.println(array1[0]);
    }
}
