package org.bryt.lection4;

public class WhileLoop {
    public static void main(String[] args) {
//        for (int k = 10; k > 0; k--) {
//            System.out.print(" " + k);
//        }

        int i = 10;
        while (i > 0){
            System.out.println(" " + i);
            i--;
            if (i == 5){
                break;
            }
        }

    }
}
