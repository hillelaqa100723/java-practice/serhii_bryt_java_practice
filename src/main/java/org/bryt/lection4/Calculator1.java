package org.bryt.lection4;

import java.awt.image.RasterOp;

public class Calculator1 {
    public static void main(String[] args) {
        int a = 15;
        int b = 0;
        // + - / * %
        char operation = '!';

        int rez = 0;

        if (operation == '+') {
            rez = a + b;
        } else if (operation == '-') {
            rez = a - b;
        } else if (operation == '*') {
            rez = a * b;
        } else if (operation == '/') {
            if (b != 0) {
                rez = a / b;
            } else {
                rez = -100000;
                System.out.println("Cannot divide by 0!");
            }
        } else if (operation == '%') {
            if (b != 0) {
                rez = a / b;
            } else {
                rez = -100000;
                System.out.println("Cannot divide by 0!");
            }
        } else {
            System.out.println("Operation is not supported!!!");
        }

        System.out.printf("Result: %s", rez);

    }


}
