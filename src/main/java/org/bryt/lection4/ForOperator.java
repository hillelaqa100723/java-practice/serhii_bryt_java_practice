package org.bryt.lection4;

public class ForOperator {
    public static void main(String[] args) {
        int i = 1;
        for (; ; ) {
            if (i <= 10) {
                System.out.print(" " + i);
                i++;
            } else {
                break;
            }
        }
        System.out.println();
        System.out.println(i);
        System.out.println();


        for (int k = 10; k > 0; k--) {
            System.out.print(" " + k);
        }

        System.out.println();
        for (int k = 1; k < 100; k++) {
            if (k % 3 == 0) {
                continue;
            }
            System.out.print(" " + k);
        }

        System.out.println();
        for (int k = 0, p = 10; k <= 10; k++, p--) {
            System.out.println(k + " " + p);
        }

    }
}
